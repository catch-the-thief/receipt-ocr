package pl.kamil_jurczak.receipt_ocr

import akka.actor.typed.ActorSystem
import akka.actor.typed.scaladsl.Behaviors
import akka.http.scaladsl.Http

object Server extends App with AppRoutes {

  implicit val system = ActorSystem(Behaviors.empty, "receipt-ocr-system")
  implicit val executionContext = system.executionContext

  val server = Http().newServerAt("localhost", 8080).bind(routes)
  server.map { _ =>
    println("Successfully started")
  } recover { case ex =>
    println("Failed to start the server due to: " + ex.getMessage)
  }
}
