package pl.kamil_jurczak.receipt_ocr

import akka.http.scaladsl.model._
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route

trait AppRoutes {
  val routes: Route =
    path("healthcheck") {
      get {
        complete(HttpEntity(ContentTypes.`text/plain(UTF-8)`, "Healthy"))
      }
    } ~
      path("receipt") {
        fileUpload("receipt") { case (fileInfo, byteSource) =>
          complete(StatusCodes.OK)
        }
      }
}
